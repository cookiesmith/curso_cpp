#include <iostream>

int soma (int * vetor, int quant)
{
	int soma = 0;
	for (int i = 0; i < quant; i++)
	{
		soma += vetor[i];
	}
	return soma;
}

float soma (float * vetor, int quant)
{
	float soma = 0;
	for (int i = 0; i < quant; i++)
	{
		soma += vetor[i];
	}
	return soma;
}


int main(void)
{
	int a[10] = {0, 10, 20, 30, 40, 50, 60, 70, 80, 90};
	float b[10] = {(float)0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9};

	std::cout << soma(a, 10) << '\n';
	std::cout << soma(b, 10) << '\n';
}

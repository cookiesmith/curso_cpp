#include <iostream>

void permuta(int * a, int * b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

int main(void)
{
	int perm = 999999999, a = 1, b = 2, i;

	//Lembre-se de definir um valor fixo para perm, e não ler da
	//entrada. O compilador precisa saber, quando compilar, quantas
	//vezes a função vai rodar.

	//std::cin >> a;
	//std::cin >> b;

	for (i = 0; i < perm; i++)
	{
		permuta(&a, &b);
	}
}

#include <iostream>

void func1(int var1 = 0, int var2 = 1, int var3 = 2)
{
	std::cout << var1 << '\n';
	std::cout << var2 << '\n';
	std::cout << var3 << '\n';
}

int main(void)
{
	int param;

	std::cin >> param;

	if (param > 3)
	{
		param = 3;
	}

	int params[param];

	for (int i = 0; i < param; i++)
	{
		std::cin >> params[i];
	}

	if (param == 3)
		func1 (params[0], params[1], params[2]);
	else if (param == 2)
		func1 (params[0], params[1]);
	else if (param == 1)
		func1 (params[0]);
	else
		func1 ();
}

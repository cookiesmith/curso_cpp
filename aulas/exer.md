# Exercícios da aula 1

Esta página é endereçada para exercitar os tópicos apresentados em sala de
aula. Aulas perguntas pertinentes, curiosidades, e pegadinhas serão
listadas aqui, separadas por tópico, e aula.

## Diferenças

Por que C é melhor que C++?

Dá pra fazer orientação a objetos usando C?

Dá pra fazer programas estruturados usando C++?

E printf? Eu consigo usar em C++? As outras bibliotecas, como stdlib, ou
time, ou math, podem ser usadas também?

## Prototipagem

Tente usar 'cout' sem declarar biblioteca... TALVEZ ler o manual do g++
possa lhe ajudar.

## COUT, CIN e CLOG

Algo semelhante é possível?

```C++

//...
variavel << std::cin;
variavel >> std::cout;
//...

```

Como 'cout' e 'cin' conseguem lidar com variáveis independentemente de
tipo?

Existe diferença entre 'cout', 'clog' e 'cerr'?

Pra que serve o 'flush'?

Dê um exemplo, em código fonte, onde usar flush provoca erros no programa.

## Uniões

Qual a diferença entre union e struct?

Dado 3 variáveis, A, B e C, em união, escreva um valor em A, apresente o
valor em A, em seguida escreva um valor em B, apresente o valor em C, e por
fim, escreva um valor em C, e apresente o valor de A. O que ocorre se elas forem de tipos
diferentes?

## Escopo de função

Me diga se algo semelhante é possível... Será que seria
possível fazer isso para um número desconhecido de chamadas pai, e ainda
mais: durante uma recursão?

```C++

#include <iostream>

teste = 0;

void func2(void)
{
	int teste = 8001;
	std::cout << teste_global << '\n';
	std::cout << teste_main << '\n';
	std::cout << teste_func1 << '\n';
	std::cout << teste_local << '\n';
}
void func1(void)
{
	int teste = 10;
	func2();
}

int main(void)
{
	int teste = 5;
	func1();
}

```

## Padronização de parâmetros


O que acontece em cada chamada:

```C++
void funcao(int par1 = a, int par2 = b, int par3 = c, int par4 = d)
{
//...
}

int main(void)
{
	int w = 10, x = 10, y = 10, z = 10;
	funcao(w, x, y, z);
	funcao(w, x, z);
	funcao(w, x, , z);
	funcao(w, x, w, z, w);
}
```

## Formatação de saída

Faça uma árvore de natal, ou uma pista de corrida tortuosa.

Pegue o caminho das pedras para entender como funciona por debaixo dos
panos (veja o asm). Tem muita diferença entre 'cout.width' e 'setw'?


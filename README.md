# Curso Intensivo de C++ e orientação à objetos

```
[allan@home ~]$ ./introduction
Bem vindo ao meu curso de C++. Prepare seu café, acione o modo dark e...

Falha de Segmentação
```

Este curso foi elaborado visando o ensino de conceitos básicos de
programação orientada à objeto, fazendo uso de C++ e aspectos internos da
linguagem.

## Tópicos

 * Introdução (Aula 1, 10/07)

 - [x] Diferenças do C
 - [x] Exemplo de programa
 - [x] Escrita e leitura
 - [x] Uniões
 - [x] Formatação de números

 * Estrutura de Dados (Aula 2, 11/07)

 - [x] Aspectos de memória
 - [x] ASM
 - [x] Final de arquivo
 - [x] Sobrecarga (?)
 - [x] Estruturas

 * Programação Orientada a Objetos (Aula 3, 17/07)

 - [x] Definição e utilidade
 - [x] Conceitos do paradigma
 - [x] Exemplo de classe
 - [x] Encapsulamento
 - [x] Construtores e Destrutores
 - [x] Operadores e Extratores
 - [x] Herança

* Exercícios

 - [ ] Yet to come

## Bibliografias

Jamsa's C/C++ Programmer's Bible
(página 452)

## Links interessantes

 * Vídeo aulas

[Aula 1](https://drive.google.com/file/d/1SHfpE12zICw7l5KgmBGQo7eKVz3Uer8I)

[Aula 2](https://drive.google.com/file/d/1-8r9avyIcT372UiPU0xSJKjQBf8O3mTK)

[Aula 3](https://drive.google.com/file/d/10iSXYRAyXjzBowDDTJYz6aO6MIpP5YCm)

[Aula 4](https://drive.google.com/file/d/1oT-lQhwfYkOfEDxbGnADUXESWL1JXhjb)

[Aula 5](https://drive.google.com/file/d/18KN9S6y9tzJV9L5cxEMv9ip5_2uHfEyA)

[Aula 6](https://drive.google.com/file/d/1aefBkpclEagl2lHG4DHPwMZJXrvAs0cw)

 * [Exercícios e soluções](aulas/exercicios)
 * [Compilador](https://gcc.gnu.org)
 * [Debbuger](https://gnu.org/software/gdb)
 * [GNU!](https://gnu.org)

## Ferramentas interessantes

Todas as ferramentas aqui utilizadas terão um manual imbutido no linux.
Basta usar o man.

```
man [termo]

# Exemplo
man man
man time
man gdb

```

Também existe o man online, e para os programas com página citada, também é
recomendado que olhe a documentação.

 * [man](http://man.he.net)

## Licença

O material produzido é gratuito. Qualquer um pode copiar, editar,
reproduzir e armazenar o presente trabalho.

## Autores

Allan Ribeiro - Estudante de Engenharia da Computação (UTFPR-PB), presidente da equipe
de robótica PatoBots, Líder do Seguidor de Linhas.

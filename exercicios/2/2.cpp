/*
entrada:
n
n*{
	nome
	medida
}

sólidos_ridiculos:
nomes não dados
medidas decimais
medidas negativas

saida:
area volume

*---------------*-----------------------*-----------------------*
|Forma          |Área S.                |Volume                 |
|---------------|-----------------------|-----------------------|
|tetraedro      |a^2*r(3)               |a^3*r(2)/12            |
|hexaedro       |6*a^2                  |a^3                    |
|octaedro       |2*a^2*r(3)             |a^3*r(2)/3             |
|dodecaedro     |3*r(25+10*a^2*r(5))    |a^3*(15+7*r(5))/4      |
|icosaedro      |a^3*5*r(3)             |a^3*5*(3+r(5))/12      |
*---------------*-----------------------*-----------------------*

class Forma
|a
|area
|vol
|-Tetraedro
||form. area
||form. vol
|-Hexaedro
||form.area
||form.vol
....


TODO:

Remover código repetido das classes
encapsular nome
(talvez) definir construtor da classe forma
Terminar classes construtoras filhas
*/

#include <iostream>
#include <string>
#include <iomanip>
#include <math.h>

#define MAX_NAM 11

class Forma
{
	public:
		float get_area();
		float get_vol();
	protected:
		int a;
		float area;
		float vol;
};

float Forma::get_area()
{
	return area;
}

float Forma::get_vol()
{
	return vol;
}

class Tetraedro : public Forma
{
	public:
		Tetraedro(int aresta);
	private:
		char nome[MAX_NAM];
};

Tetraedro::Tetraedro(int aresta)
{
	char tetra[MAX_NAM] = "Tetraedro";
	for (int i = 0; i < 10; i++)
	{	
		Tetraedro::nome[i] = tetra[i];
	}
	Tetraedro::a = aresta;
	Tetraedro::area = (float)(pow(a, 2))*(sqrt(3));
	Tetraedro::vol = (float)(pow(a, 3))*(sqrt(2))/((float)12); //Problemas na conversão
}

class Hexaedro : public Forma
{
        	public:
		Tetraedro(int aresta);
	private:
		char nome[MAX_NAM];
        
}
Hexaedro::Hexaedro()
{
	char hexa[MAX_NAM] = "Hexaedro";
	for (int i = 0; i < 10; i++)
	{	
		Hexaedro::nome[i] = hexa[i];
	}
	Hexaedro::a = aresta;
	Hexaedro::area = (float)(pow(a, 2))*(6);
	Hexaedro::vol = (float)pow(a, 3); //Problemas na conversão
}
class Octaedro : public Forma
{
		public:
		Tetraedro(int aresta);
	private:
		char nome[MAX_NAM];

}
Octaedro::Octaedro()
{
	char octa[MAX_NAM] = "Octaedro";
	for (int i = 0; i < 10; i++)
	{	
		Octaedro::nome[i] = octa[i];
	}
	Octaedro::a = aresta;
	Octaaedro::area = (float)(pow(a, 2))*(sqrt(3))*2;
	Octaedro::vol = (float)pow(a, 3)*(sqrt(2))/3; //Problemas na conversão
}
class Dodecaedro : public Forma
{
	public:
		Tetraedro(int aresta);
	private:
		char nome[MAX_NAM];

}
Dodecaedro::Dodecaedro
{
	char dode[MAX_NAM] = "Dodecaedro";
	for (int i = 0; i < 10; i++)
	{	
		Dodecaedro::nome[i] = dode[i];
	}
	Dodecaedro::a = aresta;
	Dodecaedro::area = (float)3*(sqrt(25+10*pow(a,2)*sqrt(5)));
	Dodecaedro::vol = (float)pow(a, 3)*(15+7*sqrt(5))/4; //Problemas na conversão
}
class Icosaedro : public Forma
{
	public:
		Tetraedro(int aresta);
	private:
		char nome[MAX_NAM];
}
Icosaedro::Icosaedro()
{
	char icos[MAX_NAM] = "Icosaedro";
	for (int i = 0; i < 10; i++)
	{	
		Icosaedro::nome[i] = icos[i];
	}
	Icosaedro::a = aresta;
	Icosaedro::area = (float)5*(pow(a,3))*sqrt(3);
	Icosaedro::vol = (float)5*pow(a, 3)*(3+sqrt(5))/12; //Problemas na conversão
}
int main(void)
{
	Tetraedro tetra = 20;
	std::cout << std::fixed << std::setprecision(4) << tetra.get_area(); //692.8203
}
